# OAuth & OpenID Connect

В /etc/hosts добавить `127.0.0.1 test.local`

В /Users/ronin/Develop/JAVA/oauth.properties добавить
`dropbox.app.key=<Dropbox Client Id>`, `dropbox.security.key=<Dropbox Client Secret>` ([Dropbox](https://www.dropbox.com/developers))

`bitbucket.app.key=<Bitbucket Client ID>`, `bitbucket.security.key=<Bitbucket Client Secret>` ([Bitbucket](https://bitbucket.org/account/user/Romanow/api)) 

`yahoo.app.key=<Yahoo Client ID>`, `yahoo.security.key=<Yahoo Client Secret>` ([Yahoo developer](https://developer.yahoo.com))

Заходить по `test.local:8880`.

Для Bitbucket требуется https, поэтому генерируем ключ:
```
keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650
```
В `application.properties` добавляем:
```
server.ssl.key-store=classpath:cert/keystore.p12
server.ssl.keyStoreType=PKCS12
server.ssl.key-store-password=tomcat
server.ssl.key-password=tomcat
```

HTTP сервер запускается на 8880, HTTPS на 8443.

Для Yahoo требуется указать домент (без порта), поэтому настраиваем nginx:
```
upstream backend {
	server 127.0.0.1:8880;
}

server {
    listen       80;
    server_name  test.local;

    location / {
    	proxy_set_header Host $host;
    	proxy_set_header X-Real-IP $remote_addr;
    	proxy_pass http://backend;
    	proxy_redirect off;
    }
}
```