var gulp = require('gulp');
var bower = require('gulp-bower');
var del = require('del');
var cleanCSS = require('gulp-clean-css');

gulp.task('default', ['clean', 'copy']);

gulp.task('bower', function() {
    return bower();
});

gulp.task('copy', ['bower'], function() {
    gulp.src(['./bower_components/*/dist/**',
              './bower_components/*/css/**',
              './bower_components/*/fonts/**'])
        .pipe(gulp.dest('src/main/resources/static/assets'))
});

gulp.task('clean', function() {
    del(['src/main/resources/static/assets']);
});