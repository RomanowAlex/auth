package ru.romanow.auth.bitbucket;

import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * Created by ronin on 29.09.16
 */
public class BitbucketApiAdapter
        implements ApiAdapter<BitbucketApi> {

    @Override
    public boolean test(BitbucketApi api) {
        return true;
    }

    @Override
    public void setConnectionValues(BitbucketApi api, ConnectionValues values) {}

    @Override
    public UserProfile fetchUserProfile(BitbucketApi api) {
        BitbucketUserInfo userInfo = api.userInfo();
        return new UserProfile(userInfo.getUuid(), userInfo.getDisplayName(), "", "", "", "");
    }

    @Override
    public void updateStatus(BitbucketApi api, String message) {}
}
