package ru.romanow.auth.bitbucket;

/**
 * Created by ronin on 29.09.16
 */
public interface BitbucketApi {

    BitbucketUserInfo userInfo();
}
