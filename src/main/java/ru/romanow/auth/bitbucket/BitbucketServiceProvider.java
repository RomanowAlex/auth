package ru.romanow.auth.bitbucket;

import org.springframework.social.oauth1.AbstractOAuth1ServiceProvider;
import org.springframework.social.oauth1.OAuth1Template;

/**
 * Created by ronin on 29.09.16
 */
public class BitbucketServiceProvider
        extends AbstractOAuth1ServiceProvider<BitbucketApi> {

    public BitbucketServiceProvider(String appId, String appSecret) {
        super(appId, appSecret, new OAuth1Template(
                appId, appSecret,
                "https://bitbucket.org/api/1.0/oauth/request_token",
                "https://bitbucket.org/site/oauth1/authorize",
                "https://bitbucket.org/api/1.0/oauth/access_token"));
    }

    @Override
    public BitbucketApi getApi(String accessToken, String secret) {
        return new BitbucketTemplate(getConsumerKey(), getConsumerSecret(), accessToken, secret);
    }
}
