package ru.romanow.auth.dropbox;

import org.springframework.social.connect.support.OAuth2ConnectionFactory;

/**
 * Created by ronin on 28.09.16
 */
public class DropboxConnectionFactory
        extends OAuth2ConnectionFactory<DropboxApi> {

    public DropboxConnectionFactory(String appId, String appSecret) {
        super("dropbox", new DropboxServiceProvider(appId, appSecret), new DropboxApiAdapter());
    }
}
