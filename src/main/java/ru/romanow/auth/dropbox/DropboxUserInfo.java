package ru.romanow.auth.dropbox;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by ronin on 28.09.16
 */
@Data
@Accessors(chain = true)
public class DropboxUserInfo {
    @JsonProperty("account_id")
    private String accountId;
    private String email;
}
