package ru.romanow.auth.dropbox;

/**
 * Created by ronin on 28.09.16
 */
public interface DropboxApi {

    DropboxUserInfo userInfo();
}
