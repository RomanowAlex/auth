package ru.romanow.auth.spring;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.security.SocialUser;

import java.util.Collections;

/**
 * Created by ronin on 29.09.16
 */
public class SimpleConnectionSignUp
        implements ConnectionSignUp {

    private InMemoryUserDetailsManager userDetailsService;

    public SimpleConnectionSignUp(InMemoryUserDetailsManager userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public String execute(Connection<?> connection) {
        UserProfile profile = connection.fetchUserProfile();
        User user = new SocialUser(profile.getId(), RandomStringUtils.randomAlphanumeric(5), Collections.emptyList());
        userDetailsService.createUser(user);
        return profile.getId();
    }
}
