package ru.romanow.auth.yahoo;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.OAuth2ServiceProvider;

/**
 * Created by romanow on 04.10.16
 */
public class YahooConnectionFactory
        extends OAuth2ConnectionFactory<YahooApi> {

    public YahooConnectionFactory(String appId, String appSecret) {
        super("yahoo", new YahooServiceProvider(appId, appSecret), new YahooApiAdapter());
    }

    @Override
    public Connection<YahooApi> createConnection(AccessGrant accessGrant) {
        OpenIdConnectAccessGrant grant = (OpenIdConnectAccessGrant) accessGrant;
        return new OpenIdConnectConnection<>(
                getProviderId(), extractProviderUserId(grant), grant.getAccessToken(),
                grant.getRefreshToken(), grant.getExpireTime(), grant.getJwtToken(),
                (OAuth2ServiceProvider<YahooApi>) getServiceProvider(),
                getApiAdapter()
        );
    }

    @Override
    public Connection<YahooApi> createConnection(ConnectionData data) {
        OpenIdConnectConnectionData connectionData = (OpenIdConnectConnectionData) data;
        return new OpenIdConnectConnection<>(connectionData, (OAuth2ServiceProvider<YahooApi>) getServiceProvider(), getApiAdapter());
    }
}
