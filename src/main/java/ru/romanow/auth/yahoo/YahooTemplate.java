package ru.romanow.auth.yahoo;

import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;
import org.springframework.social.support.URIBuilder;

import java.net.URI;

/**
 * Created by romanow on 04.10.16
 */
public class YahooTemplate
        extends AbstractOAuth2ApiBinding
        implements YahooApi {

    public YahooTemplate(String accessToken) {
        super(accessToken);
    }

    @Override
    public YahooUserInfo userInfo() {
        return null;
    }
}
