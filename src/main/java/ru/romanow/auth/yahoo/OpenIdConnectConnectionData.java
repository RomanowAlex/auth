package ru.romanow.auth.yahoo;

import org.springframework.social.connect.ConnectionData;

import java.util.Map;

/**
 * Created by romanow on 04.10.16
 */
public class OpenIdConnectConnectionData
        extends ConnectionData {
    private final Map<String, Object> jwtClaims;

    public OpenIdConnectConnectionData(String providerId, String providerUserId,
            String displayName, String profileUrl, String imageUrl,
            String accessToken, String secret, String refreshToken, Long expireTime,
            Map<String, Object> jwtClaims) {
        super(providerId, providerUserId, displayName, profileUrl,
              imageUrl, accessToken, secret, refreshToken, expireTime);
        this.jwtClaims = jwtClaims;
    }

    public Map<String, Object> getJwtClaims() {
        return jwtClaims;
    }
}
