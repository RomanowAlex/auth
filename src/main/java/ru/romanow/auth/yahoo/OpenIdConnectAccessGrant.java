package ru.romanow.auth.yahoo;

import org.springframework.social.oauth2.AccessGrant;

/**
 * Created by romanow on 04.10.16
 */
public class OpenIdConnectAccessGrant
        extends AccessGrant {
    private final String jwtToken;

    public OpenIdConnectAccessGrant(String accessToken, String jwtToken) {
        super(accessToken);
        this.jwtToken = jwtToken;
    }

    public OpenIdConnectAccessGrant(String accessToken, String scope, String refreshToken, Long expiresIn, String jwtToken) {
        super(accessToken, scope, refreshToken, expiresIn);
        this.jwtToken = jwtToken;
    }

    public String getJwtToken() {
        return jwtToken;
    }
}
