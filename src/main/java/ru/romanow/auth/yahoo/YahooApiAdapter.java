package ru.romanow.auth.yahoo;

import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * Created by romanow on 04.10.16
 */
public class YahooApiAdapter
        implements ApiAdapter<YahooApi> {

    @Override
    public boolean test(YahooApi api) {
        return false;
    }

    @Override
    public void setConnectionValues(YahooApi api, ConnectionValues values) {

    }

    @Override
    public UserProfile fetchUserProfile(YahooApi api) {
        return null;
    }

    @Override
    public void updateStatus(YahooApi api, String message) {

    }
}
