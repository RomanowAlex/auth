package ru.romanow.auth.yahoo;

import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.support.OAuth2Connection;
import org.springframework.social.oauth2.OAuth2ServiceProvider;

import java.text.ParseException;
import java.util.Map;

/**
 * Created by romanow on 04.10.16
 */
public class OpenIdConnectConnection<A>
        extends OAuth2Connection<A> {
    private static final Logger logger = LoggerFactory.getLogger(OpenIdConnectConnection.class);

    private Map<String, Object> jwtClaims;
    private String accessToken;
   	private String refreshToken;
   	private Long expireTime;

    public OpenIdConnectConnection(String providerId, String providerUserId,
            String accessToken, String refreshToken, Long expireTime, String jwtToken,
            OAuth2ServiceProvider<A> serviceProvider, ApiAdapter<A> apiAdapter) {
        super(providerId, providerUserId, accessToken, refreshToken, expireTime, serviceProvider, apiAdapter);

        this.accessToken = accessToken;
      	this.refreshToken = refreshToken;
        this.expireTime = expireTime;
        try {
            this.jwtClaims = SignedJWT.parse(jwtToken)
                                    .getJWTClaimsSet()
                                    .getClaims();

        } catch (ParseException exception) {
            logger.error("", exception);
        }
    }

    public OpenIdConnectConnection(OpenIdConnectConnectionData data, OAuth2ServiceProvider<A> serviceProvider, ApiAdapter<A> apiAdapter) {
        super(data, serviceProvider, apiAdapter);
    }

    @Override
    public UserProfile fetchUserProfile() {
        return new UserProfile((String)jwtClaims.get("sub"),
                               (String)jwtClaims.get("name"),
                               (String)jwtClaims.get("given_name"),
                               (String)jwtClaims.get("family_name"),
                               (String)jwtClaims.get("email"),
                               (String)jwtClaims.get("name"));
    }

    @Override
    public OpenIdConnectConnectionData createData() {
        synchronized (getMonitor()) {
            return new OpenIdConnectConnectionData(getKey().getProviderId(),
                                                   getKey().getProviderUserId(),
                                                   getDisplayName(),
                                                   getProfileUrl(),
                                                   getImageUrl(),
                                                   accessToken,
                                                   null,
                                                   refreshToken,
                                                   expireTime,
                                                   jwtClaims);
        }
    }
}
