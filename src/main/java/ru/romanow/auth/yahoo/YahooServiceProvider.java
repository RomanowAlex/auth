package ru.romanow.auth.yahoo;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Template;

/**
 * Created by romanow on 04.10.16
 */
public class YahooServiceProvider
        extends AbstractOAuth2ServiceProvider<YahooApi> {

    public YahooServiceProvider(String appId, String appSecret) {
        super(getOAuth2Template(appId, appSecret));
    }

    private static OAuth2Template getOAuth2Template(String appId, String appSecret) {
        OAuth2Template oAuth2Template = new YahooOpenIdConnectTemplate(
                appId, appSecret,
                "https://api.login.yahoo.com/oauth2/request_auth",
                "https://api.login.yahoo.com/oauth2/get_token");
        oAuth2Template.setUseParametersForClientAuthentication(true);
        return oAuth2Template;
    }

    @Override
    public YahooApi getApi(String accessToken) {
        return new YahooTemplate(accessToken);
    }
}
